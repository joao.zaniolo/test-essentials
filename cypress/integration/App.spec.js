it('should be able to sum two values', function () {
  cy.visit('http://localhost:3000/');

  cy.get('input#a').type(3);
  cy.get('input#b').type(2);

  cy.get('button[type="submit"]').click();

  cy.get('#result').should('have.text', '5');
});
