# Test Essentials

A Project to enhance my skills with automated tests.

## Tests

- **Unit** test can be found [here](./src/utils/sum.test.js).
- **Integration** test can be found [here](./cypress/integration/App.spec.js).

## Tools

- React
- @testing-library
- Jest
- Cypress
