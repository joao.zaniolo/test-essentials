import { sum } from './sum';

it('should be able to sum two values', function () {
  const [a, b] = [2, 3];

  const result = sum(a, b);

  expect(result).toBe(5);
});
