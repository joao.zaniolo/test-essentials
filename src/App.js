import { useRef, useState } from 'react';

import { sum } from './utils/sum';
import './App.css';

export function App() {
  const inputARef = useRef(null);
  const inputBRef = useRef(null);
  const [result, setResult] = useState(0);

  function handleSubmit(event) {
    event.preventDefault();

    const a = Number(inputARef.current?.value);
    const b = Number(inputBRef.current?.value);

    setResult(sum(a, b));
  }

  return (
    <div>
      <div>
        <h1>Sum</h1>
        <p>
          Result: <span id="result">{result}</span>
        </p>
      </div>
      <form onSubmit={handleSubmit}>
        <input id="a" ref={inputARef} type="number" />
        <input id="b" ref={inputBRef} type="number" />
        <button type="submit">Calculate</button>
      </form>
    </div>
  );
}
